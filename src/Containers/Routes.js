import React from 'react'
import { Route, Switch } from 'react-router-dom'
import LoginPage from './Login'
import MainPage from './Main'

function Routes() {
    return (
        <Switch>
            <Route exact path="/" component={LoginPage} />
            <Route  component={MainPage} />
        </Switch>
    )
}
export default Routes