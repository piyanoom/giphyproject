import React from 'react';
import { Card } from 'antd'
import TextTruncate from 'react-text-truncate';
import {connect} from 'react-redux'
const { Meta } = Card;

const mapDispathchToProps = dispatch =>{
    return {
        onItemMovieClick: item => {
            console.log('itemmoviedispatch', item)
            dispatch({
                type: 'click_item',
                payload: item
            })
        }
    }
}



function ItemMovie(props) {
    const item = props.item
    return (

        <Card
            onClick={() => {
                props.onItemMovieClick(item);
            }}
            hoverable
            cover={<img alt="example" src={item.images.fixed_width.url} />}

        >
            <Meta

                title={item.title}
                description={
                    <TextTruncate
                        line={1}
                        truncteText="_"
                        text={item.overview}
                        textTruncateChild={<a href="#">Read more</a>}
                    />
                }

            />
        </Card>
    )
}

export default connect(
    null,
    mapDispathchToProps)(ItemMovie)
